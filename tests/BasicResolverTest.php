<?php

namespace OneOfZero\MimeResolver\Tests;

use OneOfZero\MimeResolver\MimeResolver;
use \PHPUnit_Framework_TestCase;

class BasicResolverTest extends PHPUnit_Framework_TestCase
{
	public function testBasics()
	{
		$mimeResolver = new MimeResolver();

		$jpegExtensions = $mimeResolver->resolveExtensions('image/jpeg');
		$this->assertCount(3, $jpegExtensions);
		$this->assertContains('jpg', $jpegExtensions);
		$this->assertContains('jpeg', $jpegExtensions);
		$this->assertContains('jpe', $jpegExtensions);

		$this->assertEquals('json', $mimeResolver->resolveExtension('application/json'));
		$this->assertEquals('application/json', $mimeResolver->resolveMimeType('json'));
		$this->assertEquals('application/json', $mimeResolver->resolveFileMimeType('file.json'));
	}
}
