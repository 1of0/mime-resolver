<?php

namespace OneOfZero\MimeResolver;

use OneOfZero\MimeResolver\Resolvers\EmbeddedApacheResolver;
use OneOfZero\MimeResolver\Resolvers\ResolverInterface;
use OneOfZero\MimeResolver\Resolvers\UnixSystemResolver;
use ReflectionClass;
use RuntimeException;

class MimeResolver
{
	private static $builtinResolvers = [
		UnixSystemResolver::class,
		EmbeddedApacheResolver::class
	];

	/**
	 * @var ResolverInterface[] $resolvers
	 */
	private $resolvers = [];

	/**
	 *
	 */
	public function __construct()
	{
		foreach (self::$builtinResolvers as $resolverClass)
		{
			if (!class_exists($resolverClass))
			{
				continue;
			}

			$resolverClass = new ReflectionClass($resolverClass);

			if (!$resolverClass->implementsInterface(ResolverInterface::class))
			{
				throw new RuntimeException("Class $resolverClass->name does not implement ProviderInterface");
			}

			/** @var ResolverInterface $instance */
			$this->resolvers[] = $resolverClass->newInstance();
		}
	}

	/**
	 * Returns an array of registered resolvers.
	 *
	 * @return ResolverInterface[]
	 */
	public function getResolvers()
	{
		return $this->resolvers;
	}

	/**
	 * Registers the provided resolver.
	 *
	 * @param ResolverInterface $resolver
	 */
	public function registerResolver(ResolverInterface $resolver)
	{
		if (!in_array($resolver, $this->resolvers))
		{
			$this->resolvers[] = $resolver;
		}
	}

	/**
	 * Removes the provided resolver from the list of registered resolvers.
	 *
	 * @param ResolverInterface $resolver
	 */
	public function removeResolver(ResolverInterface $resolver)
	{
		if (!in_array($resolver, $this->resolvers))
		{
			return;
		}

		unset($this->resolvers[array_search($resolver, $this->resolvers)]);
	}

	/**
	 * Resolves the mime-type for the provided path.
	 *
	 * @param string $path
	 *
	 * @return string|null
	 */
	public function resolveFileMimeType($path)
	{
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		return $this->resolveMimeType($extension);
	}

	/**
	 * Resolves the mime-type for the provided file extension. If the mime-type cannot be resolved, null will be
	 * returned.
	 *
	 * @param string $extension
	 *
	 * @return string|null
	 */
	public function resolveMimeType($extension)
	{
		foreach ($this->resolvers as $provider)
		{
			$found = $provider->findMimeType($extension);
			if ($found !== null)
			{
				return $found;
			}
		}
		return null;
	}

	/**
	 * Resolves all mime-types for the provided file extension.
	 *
	 * @param string $extension
	 *
	 * @return string[]
	 */
	public function resolveAllMimeTypes($extension)
	{
		$results = [];
		foreach ($this->resolvers as $provider)
		{
			$found = $provider->findAllMimeTypes($extension);
			if ($found !== null)
			{
				$results = array_merge($results, $found);
			}
		}
		return array_unique($results);
	}

	/**
	 * Resolves the file extension for the provided mime-type. If the file extension cannot be resolved, null will be
	 * returned.
	 *
	 * @param string $mimeType
	 *
	 * @return string|null
	 */
	public function resolveExtension($mimeType)
	{
		foreach ($this->resolvers as $provider)
		{
			$found = $provider->findExtension($mimeType);
			if ($found !== null)
			{
				return $found;
			}
		}
		return null;
	}

	/**
	 * Resolves all file extensions for the provided mime-type.
	 *
	 * @param string $mimeType
	 *
	 * @return string[]
	 */
	public function resolveExtensions($mimeType)
	{
		$results = [];
		foreach ($this->resolvers as $provider)
		{
			$found = $provider->findAllExtensions($mimeType);
			if ($found !== null)
			{
				$results = array_merge($results, $found);
			}
		}
		return array_unique($results);
	}
}