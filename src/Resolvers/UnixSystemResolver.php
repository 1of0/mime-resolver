<?php

namespace OneOfZero\MimeResolver\Resolvers;

/**
 * Class UnixSystemProvider
 *
 * Type file resolver that attempts to load the mapping from "/etc/mime.types".
 *
 * @package OneOfZero\MimeResolver\Resolvers
 */
class UnixSystemResolver extends AbstractTypeFileResolver
{
	const MIME_FILE = '/etc/mime.types';

	/**
	 * {@inheritdoc}
	 */
	protected function getFilePath()
	{
		return self::MIME_FILE;
	}
}