<?php

namespace OneOfZero\MimeResolver\Resolvers;

/**
 * Class AbstractTypeFileProvider
 *
 * Abstract resolver that reads mime/extension mappings from a text file with the format:
 * [mime-type]      [extension] [optional second extension]
 *
 * @package OneOfZero\MimeResolver\Resolvers
 */
abstract class AbstractTypeFileResolver implements ResolverInterface
{
	const SEARCH_BY_EXT = 0;
	const SEARCH_BY_MIME = 1;

	/**
	 * Should return the path to a types file.
	 *
	 * @return string
	 */
	protected abstract function getFilePath();

	/**
	 * {@inheritdoc}
	 */
	public function findMimeType($extension)
	{
		return $this->parseFile($extension, self::SEARCH_BY_EXT, false);
	}

	/**
	 * {@inheritdoc}
	 */
	public function findAllMimeTypes($extension)
	{
		return $this->parseFile($extension, self::SEARCH_BY_EXT, true);
	}

	/**
	 * {@inheritdoc}
	 */
	public function findExtension($mimeType)
	{
		return $this->parseFile($mimeType, self::SEARCH_BY_MIME, false);
	}

	/**
	 * {@inheritdoc}
	 */
	public function findAllExtensions($mimeType)
	{
		return $this->parseFile($mimeType, self::SEARCH_BY_MIME, true);
	}

	/**
	 * @param string $needle
	 * @param int $searchBy
	 * @param bool $multiResult
	 *
	 * @return string[]|string|null
	 */
	private function parseFile($needle, $searchBy, $multiResult)
	{
		$path = $this->getFilePath();
		if (!file_exists($path) || !is_readable($path))
		{
			return null;
		}

		$stream = fopen($path, 'r');

		try
		{
			$results = [];

			// Read line by line
			while (($line = fgets($stream)) !== false)
			{
				// Skip empty and commented lines
				if (!trim($line) || trim($line)[0] === '#')
				{
					continue;
				}

				// Split by horizontal spacing
				$matches = preg_split('/\s+/', $line, -1, PREG_SPLIT_NO_EMPTY);

				// The mime-type will be the first match
				$mimeType = array_shift($matches);

				// Other matches are mapped extensions; skip if there are none for this mime-type
				if (!$matches)
				{
					continue;
				}

				// Search by mime-type? And mime-type matches?
				if ($searchBy === self::SEARCH_BY_MIME && $mimeType == $needle)
				{
					if ($multiResult)
					{
						$results = array_merge($results, $matches);
					}
					else
					{
						return $matches[0];
					}
				}

				// Search by extension? And extension in mapped extensions?
				if ($searchBy === self::SEARCH_BY_EXT && in_array($needle, $matches))
				{
					if ($multiResult)
					{
						$results[] = $mimeType;
					}
					else
					{
						return $mimeType;
					}
				}
			}

			return $multiResult ? $results : null;
		}
		finally
		{
			fclose($stream);
		}
	}
}