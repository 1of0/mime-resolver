<?php

namespace OneOfZero\MimeResolver\Resolvers;

/**
 * Class EmbeddedApacheProvider
 *
 * Type file resolver that uses a copy of the Apache mime.types list that is distributed with this library.
 *
 * @package OneOfZero\MimeResolver\Resolvers
 */
class EmbeddedApacheResolver extends AbstractTypeFileResolver
{
	const MIME_FILE = __DIR__ . '/../Static/apache-mime.types';

	/**
	 * {@inheritdoc}
	 */
	protected function getFilePath()
	{
		return self::MIME_FILE;
	}
}