<?php

namespace OneOfZero\MimeResolver\Resolvers;

/**
 * Interface ProviderInterface
 *
 * Defines an interface for a resolver that resolves extension from mime-types and mime-types from file extensions.
 *
 * @package OneOfZero\MimeResolver\Resolvers
 */
interface ResolverInterface
{
	/**
	 * Should search and return the first mime-type associated with the provided file extension.
	 *
	 * If no association is found, null should be returned.
	 *
	 * @param string $extension
	 *
	 * @return null|string
	 */
	public function findMimeType($extension);

	/**
	 * Should search and return the all mime-types associated with the provided file extension.
	 *
	 * If no associations are found, an empty array should be returned.
	 *
	 * @param string $extension
	 *
	 * @return string[]
	 */
	public function findAllMimeTypes($extension);

	/**
	 * Should search and return the first file extension associated with the provided mime-type.
	 *
	 * If no association is found, null should be returned.
	 *
	 * @param string $mimeType
	 *
	 * @return null|string
	 */
	public function findExtension($mimeType);

	/**
	 * Should search and return the all file extensions associated with the provided mime-type.
	 *
	 * If no associations are found, an empty array should be returned.
	 *
	 * @param string $mimeType
	 *
	 * @return string[]
	 */
	public function findAllExtensions($mimeType);
}